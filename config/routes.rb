Rails.application.routes.draw do
  get 'new/index'
  get 'welcome/index'
  
  resources :articles do
  	resources :comments
  end
  
  root 'welcome#index'

  
end
